**1）推荐：**

1.原生ajax

2.fetch

fetch号称是ajax的替代品，它的api是基于Promise设计的；旧版的浏览器不支持fecth，需要使用polyfill es6-promise

3.axios（目前基本上各个框架都在使用）

特性：从浏览器中创建XMLHttpRequest;从node.js中发出http请求；支持Promise API；拦截请求和响应；转换请求和响应数据；取消请求；自动转换json数据；客户端支持防止CSRF/XSRF

支持多种请求方式：axios(config) 、axios.request(config)、axios.get(get[,config])、axios.delete(url[,config])、axios.head(url[,config])、axios.post(url[data[,config]])、axios.put(url[data[,config]])...

名称由来：ajax i/o system

测试接口：

**2）使用：**

1.安装axios：npm install axios

2.简单使用：get请求操作：参数用params ; post请求操作：参数用data

3.并发请求：

axios.all，可以放入多个请求数组；axios.all返回的结果是一个数组，可以使用axios.spread将数组的值展开

4.常用的配置选项

- 请求地址 url:'/itlike'
- 请求类型 method:'get'
- 请求根路径 baseURL:'http://www.itlike.com'
- 请求前数据处理 transformRequest:[function(data,headers){return data;}]
- 请求后数据处理 transformResponse:[function(data,headers){return data;}]
- 自定义请求头 headers:{'X-Requested-With':'XMLHttpRequest'}
- URL查询对象 params:{ID:'12345'} 和get对应
- URL查询对象序列化 paramsSerializer:function(data){return QS.stringify(params,{arrayFormat:'brackets'})}
- request body data:{firstName:"zhu"} 和post对应
- 超时设置 timeout:1000 ms
- 跨域是否带token withCredentials:false
- 自定义请求处理
- 身份验证 auth:{username:"aaa",password:"123456"}
- 响应数据规则 responseType:"json" ,"arraybuffer","document","text","stream"

5.全局配置

```js
axios.defaults.baseURL = "http://demo.itlike.com/web/xlmc";
axios.defaults.timeout = 5000;
axios.defaults.headers.post['Content-Type'] = "application/x-www-form-urlencode";
...
```

 







