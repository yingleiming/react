```js

https://www.liaoxuefeng.com/wiki/1022910821149312/1031549578462080
//如果是单个参数 x=>x*x
var fn=x=>x*x;
console.log(fn(10));//100
//等价于
var fn = function (x) {
    return x*x;
}
console.log(fn(20));//400


var yn = x=>{
    if(x>0){
        return x*x;
    }else{
        return -x*x;
    }
}
console.log(yn(-10));//-100
//等价于
var yn=function (x) {
    if(x>0){
        return x*x;
    }else{
        return -x*x;
    }
}
console.log(yn(-20));//-400


//如果是多个参数，就用 () 括起来 (x,y)=>x-y;

var mn=(x,y)=>x-y;
console.log(mn(15,10));//5
//等价于
function mn(x,y) {
    return x-y
}
console.log(mn(25,3));//22

//如果是无参数 ()=>3.14;

var nn =()=>3.14;
console.log(nn());//3.14
//等价于
function nn() {
    return 3.15
}
console.log(nn());//3.15

//可变参数 (x,y,...rest)=>{}
var on=(x,y,...rest)=>{
    var i,sum=x+y;
    for(i=0;i<rest.length;i++){
        sum+=rest[i]
    }
    return sum;
}

console.log(on(1,2,3,4));//10
//等价于
function on(x,y,...rest) {
    var i,sum=x+y;
    for(i=0;i<rest.length;i++){
        sum+=rest[i]
    }
    return sum;
}
console.log(on(5,6,7,8));//26
'use strict'
//如果要返回一个对象，就要注意，如果是单表达式，这么写的话会报错
var pn=(name)=>{foo:name}
console.log(pn("李家明"));//undefined
//因为和函数体有语法冲突，所以要改成下面的形式
var rn=(name)=>({foo:name})
console.log(rn("李家明"));//{foo: "李家明"}




/**
 * this
 * 箭头函数看上去是匿名函数的一种简写，但实际上，箭头函数和匿名函数有个明显的区别：箭头函数内部的 this 是词法作用域，由上下文决定
 * 由于 JavaScript 函数对 this 绑定的错误处理，下面的例子无法得到预期结果
 * 
 */

var obj = {
    birthDay:1990,
    getAge:function () {//匿名函数
        var b = this.birthDay;
        console.log(b);//1990
        var fn = function () {//匿名函数
            return new Date().getFullYear() - this.birthDay; //this 指向 window
        }
        return fn();
    }
}
obj.getAge();//NaN

//所以经常看到这种 hack 写法
var obj = {
    birthDay:1990,
    getAge:function () {
        var that = this;
        var fn = function () {
            return new Date().getFullYear() - that.birthDay;
        }
        return fn();
    }
}
obj.getAge();


//使用箭头函数 就不需要这种 hack 了
var obj = {
    birthDay:1990,
    getAge:function () {//匿名函数
        var b = this.birthDay;
        console.log(b);//1990
        var fn=()=>new Date().getFullYear() - this.birthDay; //this 指向 obj
        return fn();
    }
}
obj.getAge();//31



//由于 this 在箭头函数中已经按照词法作用域绑定了，所以，用 call() 或者 apply() 调用箭头函数时，无法对 this 进行绑定，即传入的第一个参数被忽略
var obj = {
    birthDay:1990,
    getAge:function (year) {//匿名函数
        var b = this.birthDay;
        console.log(b);//1990
        var fn=(y)=>y - this.birthDay; //this 指向 obj
        return fn.call({birthDay:2000},year);
    }
}
obj.getAge(2015);//25
```

